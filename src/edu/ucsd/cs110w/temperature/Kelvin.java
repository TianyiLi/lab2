/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110wcc): write class javadoc
 *
 * @author cs110wcc
 */
public class Kelvin extends Temperature {

	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + this.getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		Kelvin k = new Kelvin(this.getValue()+273);
		return k;
	}
	@Override
	public Temperature toFahrenheit() {
		Fahrenheit f = new Fahrenheit((((this.getValue()-273)*9)/5)+32);
		return f;
	}
	public Temperature toKelvin() {
		// TODO: Complete this method
		return this;
	}

}
