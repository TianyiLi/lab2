package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue()+ " F";
	}
	@Override
	public Temperature toCelsius() {
		Celsius c = new Celsius((this.getValue()-32)*5/9);
		return c;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	public Temperature toKelvin() {
		// TODO: Complete this method
		Kelvin k = new Kelvin((this.getValue()-32)*5/9-273);
		return k;
	}
}
